# spring-boot-stockManagement

This project was created to provide a small tutorial for integrating Spring Boot with several JEE technologies.

The code was developed by Luis Ribeiro and it provides no warranties what so ever.

In order to correctly and automatically deploy the code, you must have Java 8 and Maven 3.5+ installed. If this is the case, you can simply execute "mvn clean package" on your project root and a Uber JAR will be generated. Then you only need to run "java -jar <nameOfTheJAR>" to execute it or execute the Main class directly on your favorite IDE.