package org.luismmribeiro.tutorials.stockmanagement.repositories;

import org.luismmribeiro.tutorials.stockmanagement.models.Product;
import org.springframework.stereotype.Repository;


@Repository
public class ProductRepository extends EntityRepository<Product> {
	@Override
	protected Class<Product> getEntityClass() {
		return Product.class;
	}
	
	@Override
	protected String getAllEntityQueryName() {
		return Product.GET_ALL_PRODUCTS_QUERY_NAME;
	}
}
