package org.luismmribeiro.tutorials.stockmanagement.business;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.luismmribeiro.tutorials.stockmanagement.models.BaseEntity;
import org.luismmribeiro.tutorials.stockmanagement.repositories.EntityRepository;

public abstract class EntityBusiness<T extends EntityRepository<R>, R extends BaseEntity> {
	@Inject
	protected T repository;
	

	@Transactional
	public void delete(long id) {
		repository.delete(id);
	}

	@Transactional
	public R update(R entity) {
		return repository.update(entity);
	}
	
	@Transactional
	public R create(R entity) {
		return repository.create(entity);
	}
	
	public List<R> getAll() {
		return repository.getAll();
	}
	
	public R findById(long id) {
		return  repository.findById(id);
	}
}
