package org.luismmribeiro.tutorials.stockmanagement.business;

import org.luismmribeiro.tutorials.stockmanagement.models.Product;
import org.luismmribeiro.tutorials.stockmanagement.repositories.ProductRepository;
import org.springframework.stereotype.Component;

@Component
public class ProductBusiness extends EntityBusiness<ProductRepository, Product> {

}
