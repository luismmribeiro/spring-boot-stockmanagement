package org.luismmribeiro.tutorials.stockmanagement.business;

import java.util.List;

import org.luismmribeiro.tutorials.stockmanagement.models.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repositories.ShelfRepository;
import org.springframework.stereotype.Component;

@Component
public class ShelfBusiness extends EntityBusiness<ShelfRepository, Shelf> {
	public List<Shelf> findByProductId(long id) {
		return repository.findByProductId(id);
	}
}
