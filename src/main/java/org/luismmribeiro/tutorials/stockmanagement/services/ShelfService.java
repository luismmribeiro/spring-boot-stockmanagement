package org.luismmribeiro.tutorials.stockmanagement.services;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.luismmribeiro.tutorials.stockmanagement.business.ShelfBusiness;
import org.luismmribeiro.tutorials.stockmanagement.models.Shelf;
import org.luismmribeiro.tutorials.stockmanagement.repositories.ShelfRepository;
import org.springframework.stereotype.Component;

@Component
@Path("shelves")
public class ShelfService extends EntityService<ShelfBusiness, ShelfRepository, Shelf> {

	@GET
	@Path("/product/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Shelf> getShelvesByProductID(@PathParam("id") long id) {
		return business.findByProductId(id);
	}
}
