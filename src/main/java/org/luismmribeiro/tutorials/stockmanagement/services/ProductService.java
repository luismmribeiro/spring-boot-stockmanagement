package org.luismmribeiro.tutorials.stockmanagement.services;

import javax.ws.rs.Path;

import org.luismmribeiro.tutorials.stockmanagement.business.ProductBusiness;
import org.luismmribeiro.tutorials.stockmanagement.models.Product;
import org.luismmribeiro.tutorials.stockmanagement.repositories.ProductRepository;
import org.springframework.stereotype.Component;

@Component
@Path("products")
public class ProductService extends EntityService<ProductBusiness, ProductRepository, Product> {
}
