package org.luismmribeiro.tutorials.stockmanagement.services;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
public class JerseyInitializer extends ResourceConfig {
	public JerseyInitializer() {
        registerEndpoints();
    }
	
    private void registerEndpoints() {
        register(ProductService.class);
        register(ShelfService.class);
    }
}

