package org.luismmribeiro.tutorials.stockmanagement.models;

import javax.persistence.Entity;
import javax.persistence.NamedQuery;

@Entity
@NamedQuery(name=Product.GET_ALL_PRODUCTS_QUERY_NAME, query="SELECT p FROM Product p")
public class Product extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	public static final String GET_ALL_PRODUCTS_QUERY_NAME = "getAllProducts";

	private double discountValue;
	private double iva ;
	private double pvp;
	
	
	public double getDiscountValue() {
		return discountValue;
	}
	public void setDiscountValue(double discountValue) {
		this.discountValue = discountValue;
	}
	public double getIva() {
		return iva;
	}
	public void setIva(double iva) {
		this.iva = iva;
	}
	public double getPvp() {
		return pvp;
	}
	public void setPvp(double pvp) {
		this.pvp = pvp;
	}

}
